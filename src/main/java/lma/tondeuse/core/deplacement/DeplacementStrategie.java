package lma.tondeuse.core.deplacement;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Interface des implementations des stratégies de déplacement. Cette stratégie
 * de déplacement est utilisée pour calculer la nouvelle position de la tondeuse
 * en fonction de l'{@link InstructionDeplacement} à effectuer, la
 * {@link PositionTondeuse} initiale et du {@link Terrain} sur lequel se déplace
 * la tondeuse.
 * 
 * @author Loïc MARIE
 *
 */
public interface DeplacementStrategie {

	/**
	 * Calcul de la nouvelle position en fonction du terrain, de la position
	 * initiale et de l'instruction.
	 * 
	 * @param instruction
	 *          Instruction de déplacement à effectuer. <b>Ne peut pas être
	 *          <code>null</code></b>
	 * @param positionInitiale
	 *          Position initiale d'où effectuer l'instruction de déplacement.
	 *          <b>Ne peut pas être <code>null</code></b>
	 * @param terrain
	 *          Le terrain sur lequel effectuer le déplacement. <b>Ne peut pas
	 *          être <code>null</code></b>
	 * @return La nouvelle position.
	 */
	PositionTondeuse calculerNouvellePosition(InstructionDeplacement instruction, PositionTondeuse positionInitiale,
	    Terrain terrain);
}
