package lma.tondeuse.core.position;

/**
 * Position déterminée par un X et un Y.
 * 
 * @author Loïc MARIE
 *
 */
public class Position {

	private final int x, y;

	/**
	 * Constructeur d'une position.
	 * 
	 * @param x
	 *          La coordonnée sur l'axe des abscisses de la position.
	 * @param y
	 *          La coordonnée sur l'axe des ordonnées de la position.
	 */
	public Position(final int x, final int y) {
		this.x = x;
		this.y = y;
	}

	/** @return La coordonnée X de la position courante. */
	public int getX() {
		return x;
	}

	/** @return La coordonnée Y de la position courante. */
	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "Position [x=" + x + ", y=" + y + "]";
	}

	/**
	 * Détermine si deux positions sont identiques (elles désignent le même
	 * emplacement).
	 * 
	 * @param other
	 *          La position à comparer.
	 * @return <code>true</code> si les positions sont identiques,
	 *         <code>false</code> sinon.
	 */
	public boolean isIdentique(Position other) {
		return other != null ? other.x == x && other.y == y : false;
	}

}
