package lma.tondeuse.core.position;

/**
 * Ensemble des directions disponibles.
 * 
 * @author Loïc MARIE
 *
 */
public enum Direction {

	/** Nord */
	N,

	/** Sud */
	S,

	/** Est */
	E,

	/** Ouest */
	W;

	/**
	 * Effectue une rotation horaire de 90° sur la direction courante.
	 * 
	 * @return Nouvelle direction obtenue suite à la rotation.
	 */
	public Direction rotationHoraire() {
		switch (this) {
		case N:
			return E;
		case S:
			return W;
		case E:
			return S;
		case W:
			return N;
		default:
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Effectue une rotation antihoraire de 90° sur la direction courante.
	 * 
	 * @return Nouvelle direction obtenue suite à la rotation.
	 */
	public Direction rotationAntiHoraire() {
		switch (this) {
		case N:
			return W;
		case S:
			return E;
		case E:
			return N;
		case W:
			return S;
		default:
			throw new UnsupportedOperationException();
		}
	}
}
