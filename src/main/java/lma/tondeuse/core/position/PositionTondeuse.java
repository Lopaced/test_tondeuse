package lma.tondeuse.core.position;

/**
 * Représentation d'une {@link Position} qui possède une {@link Direction}.
 * 
 * @author Loïc MARIE
 *
 */
public class PositionTondeuse extends Position {

	private final Direction direction;

	/**
	 * Constructeur.
	 * 
	 * @param x
	 *          La coordonnée sur l'axe des abscisses de la position.
	 * @param y
	 *          La coordonnée sur l'axe des ordonnées de la position.
	 * @param direction
	 *          La direction de la nouvelle position.
	 */
	public PositionTondeuse(final int x, final int y, final Direction direction) {
		super(x, y);
		this.direction = direction;
	}

	/** @return La direction de la position courante. */
	public Direction getDirection() {
		return direction;
	}

	@Override
	public String toString() {
		return "Position [x=" + getX() + ", y=" + getY() + ", direction=" + direction + "]";
	}
}
