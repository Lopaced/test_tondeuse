package lma.tondeuse.core;

import static org.apache.commons.lang.Validate.notNull;

import java.util.ArrayList;
import java.util.List;

import lma.tondeuse.core.deplacement.DeplacementStrategie;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.observateur.DebutInstructionObservable;
import lma.tondeuse.core.observateur.DebutInstructionObservateur;
import lma.tondeuse.core.observateur.FinInstructionObservable;
import lma.tondeuse.core.observateur.FinInstructionObservateur;
import lma.tondeuse.core.observateur.FinTraitementObseravable;
import lma.tondeuse.core.observateur.FinTraitementObservateur;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.EtatParcelle;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Une tondeuse effectue un ensemble d'{@link InstructionDeplacement}
 * interprétés par une {@link DeplacementStrategie} pour se déplacer sur le
 * {@link Terrain}. La tondeuse possède une {@link PositionTondeuse} qui la
 * situe sur le {@link Terrain}.
 * 
 * @author Loïc MARIE
 *
 */
public class Tondeuse implements DebutInstructionObservable, FinInstructionObservable, FinTraitementObseravable {

	private final Terrain terrainATondre;
	private final DeplacementStrategie deplacementStrategie;
	private PositionTondeuse positionCourante;
	private List<FinTraitementObservateur> observateursFinTraitement;
	private List<DebutInstructionObservateur> observateursDebutInstruction;
	private List<FinInstructionObservateur> observateursFinInstruction;

	/**
	 * Constructeur
	 * 
	 * @param terrainATondre
	 *          Le terrain à tondre. <b>Ne peut pas être <code>null</code></b>
	 * @param deplacementStrategie
	 *          La stratégie de déplacement. <b>Ne peut pas être <code>null</code>
	 *          </b>
	 * @param positionInitiale
	 *          La position initiale de la tondeuse sur le terrain. <b>Ne peut pas
	 *          être <code>null</code></b>
	 */
	public Tondeuse(final Terrain terrainATondre, final DeplacementStrategie deplacementStrategie,
	    final PositionTondeuse positionInitiale) {

		notNull(terrainATondre, "terrainATondre ne doit pas être null");
		notNull(deplacementStrategie, "deplacementStrategie ne doit pas être null");
		notNull(positionInitiale, "positionInitiale ne doit pas être null");

		if (!terrainATondre.estAccessible(positionInitiale)) {
			throw new IllegalArgumentException("La position initiale de la tondeuse n'est pas accessible");
		}

		this.terrainATondre = terrainATondre;
		this.deplacementStrategie = deplacementStrategie;
		positionCourante = positionInitiale;

		observateursFinTraitement = new ArrayList<FinTraitementObservateur>();
		observateursFinInstruction = new ArrayList<FinInstructionObservateur>();
		observateursDebutInstruction = new ArrayList<DebutInstructionObservateur>();

		// On identifie la parcelle comme occupée
		terrainATondre.setEtatParcelle(EtatParcelle.OCCUPEE, positionInitiale);
	}

	@Override
	public void addFinTraitementObservateur(FinTraitementObservateur observateur) {
		notNull(observateur, "L'obsertateur ne doit pas être null");
		observateursFinTraitement.add(observateur);
	}

	@Override
	public void removeFinTraitementObservateur(FinTraitementObservateur observateur) {
		notNull(observateur, "L'obsertateur ne doit pas être null");
		observateursFinTraitement.remove(observateur);
	}

	@Override
	public void addFinInstructionObservateur(FinInstructionObservateur observateur) {
		notNull(observateur, "L'obsertateur ne doit pas être null");
		observateursFinInstruction.add(observateur);
	}

	@Override
	public void removeFinInstructionObservateur(FinInstructionObservateur observateur) {
		notNull(observateur, "L'obsertateur ne doit pas être null");
		observateursFinInstruction.remove(observateur);
	}

	@Override
	public void addDebutInstructionObservateur(DebutInstructionObservateur observateur) {
		notNull(observateur, "L'obsertateur ne doit pas être null");
		observateursDebutInstruction.add(observateur);
	}

	@Override
	public void removeDebutInstructionObservateur(DebutInstructionObservateur observateur) {
		notNull(observateur, "L'obsertateur ne doit pas être null");
		observateursDebutInstruction.remove(observateur);
	}

	/**
	 * Effectue séquentiellement l'ensemble des instructions de l'
	 * {@link Iterable} passé en paramètre. <br>
	 * Une notification est envoyée :
	 * <ul>
	 * <li>avant le début de traitement d'une instruction, à chacun des
	 * observateurs ajoutés via
	 * <code>{@link #addDebutInstructionObservateur(DebutInstructionObservateur)}</code>
	 * .</li>
	 * <li>
	 * après le traitement d'une instruction, à chacun des observateurs ajoutés
	 * via
	 * <code>{@link #addFinInstructionObservateur(FinInstructionObservateur)}</code>
	 * .</li>
	 * <li>
	 * une fois que toutes les instructions ont été traitées, à chacun des
	 * observateurs ajoutés via
	 * <code>{@link #addFinTraitementObservateur(FinTraitementObservateur)}</code>
	 * .</li>
	 * <ul>
	 * 
	 * @param instructions
	 *          Les instructions à traiter.
	 */
	public void effectuerInstructions(final Iterable<InstructionDeplacement> instructions) {

		notNull(instructions, "Le paramètre ne doit pas être null");

		instructions.forEach(instruction -> effectuerInstruction(instruction));

		// Notification des observateurs de la fin de traitement
		observateursFinTraitement.forEach(observateur -> observateur.traitementEffectue(positionCourante));
	}

	private void effectuerInstruction(final InstructionDeplacement instruction) {

		// Notification des observateurs du debut du traitement de l'instruction
		observateursDebutInstruction.forEach(observateur -> observateur
		    .instructionAEffecuter(instruction, positionCourante));

		PositionTondeuse nouvellePosition = deplacementStrategie.calculerNouvellePosition(instruction, positionCourante,
		    terrainATondre);

		terrainATondre.setEtatParcelle(EtatParcelle.TONDUE, positionCourante);
		terrainATondre.setEtatParcelle(EtatParcelle.OCCUPEE, nouvellePosition);

		positionCourante = nouvellePosition;

		// Notification des observateurs de la fin de l'instruction
		observateursFinInstruction.forEach(observateur -> observateur.instructionEffecutee(instruction, positionCourante));
	}

}
