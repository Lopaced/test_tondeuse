package lma.tondeuse.core.instruction;

/**
 * Les instructions de déplacement intelligibles par le système.
 * 
 * @author Loïc MARIE
 *
 */
public enum InstructionDeplacement {

	/** Gauche (soit un quart de tour vers la gauche) */
	G,

	/** Droite (soit un quart de tour vers la droite) */
	D,

	/** Avancer */
	A,

	/** Reculer */
	R
}
