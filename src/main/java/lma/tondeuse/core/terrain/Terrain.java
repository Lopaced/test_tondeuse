package lma.tondeuse.core.terrain;

import lma.tondeuse.core.position.Position;

/**
 * Terrain composé de parcelles.
 * 
 * @author Loïc MARIE
 *
 */
public interface Terrain {

	/** @return La position du bord inférieur gauche du terrain. */
	Position getBordInferieurGauche();

	/** @return La position du bord supérieur droit du terrain. */
	Position getBordSuperieurDroit();

	/**
	 * Determine si la position passée en paramètre se trouve dans les limites du
	 * terrain et est accessible.
	 * 
	 * @param position
	 *          La position à verifier. <b>Ne peut pas être <code>null</code></b>.
	 * @return <code>true</code> si la position donnée se trouve dans le terrain,
	 *         <code>false</code> sinon.
	 */
	boolean estAccessible(final Position position);

	/**
	 * Donne l'{@link EtatParcelle} de la parcelle à la {@link Position} donnée.
	 * 
	 * @param position
	 *          La position considérée. <b>Ne peut pas être <code>null</code></b>.
	 * @return L'état de la parcelle.
	 */
	EtatParcelle getEtatParcelle(final Position position);

	/**
	 * Fixe l'{@link EtatParcelle} donné à la parcelle située à la
	 * {@link Position} donnée.
	 * 
	 * @param etat
	 *          L'état de la parcelle. <b>Ne peut pas être <code>null</code></b>.
	 * @param position
	 *          La position de la parcelle considérée. <b>Ne peut pas être
	 *          <code>null</code></b>.
	 */
	void setEtatParcelle(EtatParcelle etat, final Position position);

}