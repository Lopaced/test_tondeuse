package lma.tondeuse.core.terrain;

/**
 * Ensemble des états de parcelles disponibles.
 * 
 * @author Loïc MARIE
 *
 */
public enum EtatParcelle {

	/** Tondue, cette parcelle est accessible */
	TONDUE(true),

	/** Non tondue, cette parcelle est accessible */
	NON_TONDUE(true),

	/** Occupée par une tondeuse, cette parcelle n'est pas accessible */
	OCCUPEE(false);

	private boolean accessible;

	private EtatParcelle(boolean accessible) {
		this.accessible = accessible;
	}

	/**
	 * Donne le status d'accebibilité de l'état courant.
	 * 
	 * @return <code>true</code> si l'état de la parcelle rend cette parcelle
	 *         accessible, <code>false</code> sinon.
	 */
	public boolean isAccessible() {
		return accessible;
	}

}
