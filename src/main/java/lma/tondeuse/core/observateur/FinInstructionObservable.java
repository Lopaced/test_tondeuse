package lma.tondeuse.core.observateur;

/**
 * Interface de l'objet observable du pattern observateur concernant l'événement
 * de fin de traitement d'une instruction.
 * 
 * @author Loïc MARIE
 *
 */
public interface FinInstructionObservable {

	/**
	 * Ajout de l'observateur à l'objet observé.
	 * 
	 * @param observateur
	 *          L'observateur à ajouter. <b>Ne peut pas être <code>null</code></b>
	 */
	void addFinInstructionObservateur(FinInstructionObservateur observateur);

	/**
	 * Retrait de l'observateur à l'objet observé.
	 * 
	 * @param observateur
	 *          L'observateur à retirer. <b>Ne peut pas être <code>null</code></b>
	 */
	void removeFinInstructionObservateur(FinInstructionObservateur observateur);
}
