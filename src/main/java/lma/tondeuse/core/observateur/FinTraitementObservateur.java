package lma.tondeuse.core.observateur;

import lma.tondeuse.core.position.PositionTondeuse;

/**
 * Interface de l'observateur d'un objet observable concernant l'événement de
 * fin de traitement de l'ensemble des instructions.
 * 
 * @author Loïc MARIE
 *
 */
@FunctionalInterface
public interface FinTraitementObservateur {

	/**
	 * Traitement effectué lorsque l'objet observable émet un événement de fin de
	 * traitement de l'ensemble des instructions.
	 * 
	 * @param positionFinale
	 *          Position finale suite au traitement de l'ensemble des
	 *          instructions.
	 */
	void traitementEffectue(PositionTondeuse positionFinale);

}
