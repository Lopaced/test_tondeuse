package lma.tondeuse.core.observateur;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;

/**
 * Interface de l'observateur d'un objet observable concernant l'événement de
 * fin de traitement d'une instruction.
 * 
 * @author Loïc MARIE
 *
 */
@FunctionalInterface
public interface FinInstructionObservateur {

	/**
	 * Traitement effectué lorsque l'objet observable émet un événement de fin de
	 * traitement d'une instruction.
	 * 
	 * @param instruction
	 *          Instruction qui a été traitée
	 * @param nouvellePosition
	 *          Position obtenue après le traitement de l'instruction.
	 */
	void instructionEffecutee(InstructionDeplacement instruction, PositionTondeuse nouvellePosition);
}
