package lma.tondeuse.core.observateur;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;

/**
 * Interface de l'observateur d'un objet observable concernant l'événement de
 * début de traitement d'une instruction.
 * 
 * @author Loïc MARIE
 *
 */
@FunctionalInterface
public interface DebutInstructionObservateur {

	/**
	 * Traitement effectué lorsque l'objet observable émet un événement de début
	 * de traitement d'une instruction.
	 * 
	 * @param instruction
	 *          Instruction qui sera traitée
	 * @param positionInitiale
	 *          Position initiale de la tondeuse avant le traitement de
	 *          l'instruction.
	 */
	void instructionAEffecuter(InstructionDeplacement instruction, PositionTondeuse positionInitiale);
}
