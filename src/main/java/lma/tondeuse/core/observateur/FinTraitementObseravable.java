package lma.tondeuse.core.observateur;

/**
 * Interface de l'objet observable du pattern observateur concernant l'événement
 * de fin de traitement de l'ensemble des instructions.
 * 
 * @author Loïc MARIE
 *
 */
public interface FinTraitementObseravable {

	/**
	 * Ajout de l'observateur à l'objet observé.
	 * 
	 * @param observateur
	 *          L'observateur à ajouter. <b>Ne peut pas être <code>null</code></b>
	 */
	void addFinTraitementObservateur(FinTraitementObservateur observateur);

	/**
	 * Retrait de l'observateur à l'objet observé.
	 * 
	 * @param observateur
	 *          L'observateur à retirer. <b>Ne peut pas être <code>null</code></b>
	 */
	void removeFinTraitementObservateur(FinTraitementObservateur observateur);
}
