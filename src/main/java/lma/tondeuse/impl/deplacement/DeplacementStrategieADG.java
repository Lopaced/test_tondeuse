package lma.tondeuse.impl.deplacement;

import static org.apache.commons.lang.Validate.notNull;
import lma.tondeuse.core.deplacement.DeplacementStrategie;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;
import lma.tondeuse.impl.deplacement.processeurs.AbstractDeplacementProcesseur;
import lma.tondeuse.impl.deplacement.processeurs.DeplacementAProcesseur;
import lma.tondeuse.impl.deplacement.processeurs.DeplacementDProcesseur;
import lma.tondeuse.impl.deplacement.processeurs.DeplacementGProcesseur;

/**
 * Stratégie de déplacement pouvant traiter les instructions intelligibles par
 * les processeurs {@link DeplacementAProcesseur},
 * {@link DeplacementGProcesseur} et {@link DeplacementDProcesseur}.
 * 
 * @author Loïc MARIE
 *
 */
public class DeplacementStrategieADG implements DeplacementStrategie {

	private final AbstractDeplacementProcesseur deplacementProcesseurChaine;

	/** Constructeur de la stratégie */
	public DeplacementStrategieADG() {

		deplacementProcesseurChaine = new DeplacementAProcesseur();
		deplacementProcesseurChaine.setProcesseurSuivant(new DeplacementGProcesseur()).setProcesseurSuivant(
		    new DeplacementDProcesseur());
	}

	@Override
	public PositionTondeuse calculerNouvellePosition(final InstructionDeplacement instruction,
	    final PositionTondeuse positionInitiale, final Terrain terrain) {

		notNull(instruction, "instruction ne doit pas être null");
		notNull(positionInitiale, "positionInitiale ne doit pas être null");
		notNull(terrain, "positionInitiale ne doit pas être null");

		return deplacementProcesseurChaine.calculerDeplacement(terrain, instruction, positionInitiale);

	}
}
