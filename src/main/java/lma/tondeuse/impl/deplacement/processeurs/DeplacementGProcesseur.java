package lma.tondeuse.impl.deplacement.processeurs;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Processeur de déplacement capable de traiter l'instruction
 * {@link InstructionDeplacement#G}.
 * 
 * @author Loïc MARIE
 *
 */
public class DeplacementGProcesseur extends AbstractDeplacementProcesseur {

	@Override
	protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
		return instructionDeplacement == InstructionDeplacement.G;
	}

	@Override
	protected PositionTondeuse calculerFuturePosition(final Terrain terrain, final InstructionDeplacement instructionDeplacement,
			PositionTondeuse positionInitiale) {

		int positionInitialeX = positionInitiale.getX();
		int positionInitialeY = positionInitiale.getY();
		Direction directionInitiale = positionInitiale.getDirection();

		Direction nouvelleDirection = directionInitiale.rotationAntiHoraire();

		return new PositionTondeuse(positionInitialeX, positionInitialeY, nouvelleDirection);

	}

}
