package lma.tondeuse.impl.deplacement.processeurs;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processeur de déplacement capable de traiter l'instruction
 * {@link InstructionDeplacement#A}.
 * 
 * @author Loïc MARIE
 *
 */
public class DeplacementAProcesseur extends AbstractDeplacementProcesseur {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeplacementAProcesseur.class);

	@Override
	protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
		return instructionDeplacement == InstructionDeplacement.A;
	}

	@Override
	protected PositionTondeuse calculerFuturePosition(final Terrain terrain,
	    final InstructionDeplacement instructionDeplacement, PositionTondeuse positionInitiale) {

		int positionInitialeX = positionInitiale.getX();
		int positionInitialeY = positionInitiale.getY();
		Direction directionInitiale = positionInitiale.getDirection();

		final PositionTondeuse nouvellePosition;

		switch (directionInitiale) {
		case N:
			nouvellePosition = new PositionTondeuse(positionInitialeX, positionInitialeY + 1, directionInitiale);
			break;
		case S:
			nouvellePosition = new PositionTondeuse(positionInitialeX, positionInitialeY - 1, directionInitiale);
			break;
		case E:
			nouvellePosition = new PositionTondeuse(positionInitialeX + 1, positionInitialeY, directionInitiale);
			break;
		case W:
			nouvellePosition = new PositionTondeuse(positionInitialeX - 1, positionInitialeY, directionInitiale);
			break;
		default:
			throw new UnsupportedOperationException();
		}

		if (terrain.estAccessible(nouvellePosition)) {
			return nouvellePosition;
		} else {
			LOGGER.debug("La position calculée n'est pas accessible");
			return positionInitiale;
		}
	}
}
