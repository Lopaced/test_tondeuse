package lma.tondeuse.impl.deplacement.processeurs;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Processeur de déplacement capable de traiter l'instruction
 * {@link InstructionDeplacement#R}. <br>
 * 
 * <b>Attention, pour le moment ce processeur n'est pas encore implémenté.</b>
 * 
 * TODO Implémenter le comportement du processeur.
 * 
 * @author Loïc MARIE
 *
 */
public class DeplacementRProcesseur extends AbstractDeplacementProcesseur {

	@Override
	protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
		return instructionDeplacement == InstructionDeplacement.R;
	}

	@Override
	protected PositionTondeuse calculerFuturePosition(final Terrain terrain,
	    final InstructionDeplacement instructionDeplacement, PositionTondeuse positionInitiale) {
		throw new UnsupportedOperationException("L'instruction R n'est pas encore implémentée");
	}
}
