package lma.tondeuse.impl.deplacement.processeurs;

import static java.text.MessageFormat.format;
import static org.apache.commons.lang.Validate.notNull;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Classe mère des processeurs ayant à charge des calculer les déplacements de
 * la tondeuse.
 * 
 * @author Loïc MARIE
 *
 */
public abstract class AbstractDeplacementProcesseur {

	/**
	 * Détermine si le processeur à la capacité de traiter le type
	 * {@link InstructionDeplacement} passé en paramètre.
	 * 
	 * @param instructionDeplacement
	 *          L'instruction considérée.
	 * @return <code>true</code> si le processeur peut traiter l'instruction,
	 *         <code>false</code> sinon.
	 */
	protected abstract boolean peutTraiter(InstructionDeplacement instructionDeplacement);

	/**
	 * Calcule la future position de la tondeuse sur le terrain après avoir
	 * effectuer l'instruction de déplacement.
	 * 
	 * @param terrain
	 *          Le terrain sur lequel se déplace la tondeuse.
	 * @param instructionDeplacement
	 *          L'instruction de déplacement à effectuer.
	 * @param positionInitiale
	 *          La position initiale de la tondeuse sur le terrain.
	 * @return La future position de la tondeuse sur le terrain une fois
	 *         l'instruction de déplacement effectuée.
	 */
	protected abstract PositionTondeuse calculerFuturePosition(Terrain terrain,
	    InstructionDeplacement instructionDeplacement, PositionTondeuse positionInitiale);

	private AbstractDeplacementProcesseur processeurSuivant;

	/**
	 * Ajoute le processeur suivant à la chaîne de responsabilité.
	 * 
	 * @param processeurSuivant
	 *          Le processeur suivant. <b>Ne peut pas être <code>null</code></b>
	 * @return Le processeur suivant passé en paramètre.
	 */
	public AbstractDeplacementProcesseur setProcesseurSuivant(AbstractDeplacementProcesseur processeurSuivant) {

		notNull(processeurSuivant, "Le processeur ne doit pas être null");

		this.processeurSuivant = processeurSuivant;
		return processeurSuivant;
	}

	/**
	 * Retourne le processeur suivant dans la chaîne de responsabilité.
	 * 
	 * @return Le processeur suivant (peut être <code>null</code>).
	 */
	public AbstractDeplacementProcesseur getProcesseurSuivant() {
		return processeurSuivant;
	}

	/**
	 * Calcule la nouvelle position de la tondeuse suite à son déplacement.
	 * 
	 * @param terrain
	 *          Le terrain sur lequel se déplace la tondeuse.
	 * @param instructionDeplacement
	 *          L'instruction de déplacement à effectuer.
	 * @param positionInitiale
	 *          La position initiale de la tondeuse sur le terrain.
	 * @return La position de la tondeuse sur le terrain une fois l'instruction de
	 *         déplacement effectuée.
	 */
	public PositionTondeuse calculerDeplacement(Terrain terrain, InstructionDeplacement instructionDeplacement,
	    PositionTondeuse positionInitiale) {

		if (!peutTraiter(instructionDeplacement)) {

			if (getProcesseurSuivant() != null) {
				return getProcesseurSuivant().calculerDeplacement(terrain, instructionDeplacement, positionInitiale);
			} else {
				throw new IllegalArgumentException(format("Instruction {0} non traitée", instructionDeplacement));
			}
		}

		return calculerFuturePosition(terrain, instructionDeplacement, positionInitiale);

	}

}
