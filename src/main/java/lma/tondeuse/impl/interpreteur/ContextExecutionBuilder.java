package lma.tondeuse.impl.interpreteur;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;

/**
 * Classe utilitaire permettant de construire le {@link ContextExecution}.
 * 
 * @author Loïc MARIE
 *
 */
public class ContextExecutionBuilder {

	private Charset charset = UTF_8;
	private InputStream instructionsStream;

	/**
	 * Lit les données depuis un fichier. Ceci n'est pas une opération finale.
	 * 
	 * @param fichier
	 *          Le fichier à lire.
	 * @return Le {@link ContextExecutionBuilder} courant.
	 * @throws FileNotFoundException
	 */
	public ContextExecutionBuilder lireDepuisFichier(File fichier) throws FileNotFoundException {
		instructionsStream = new FileInputStream(fichier);
		return this;
	}

	/**
	 * Lit les données depuis un fichier dans le classpath. Ceci n'est pas une
	 * opération finale.
	 * 
	 * @param fichier
	 *          Le nom du fichier contenu dans le classpath.
	 * @return Le {@link ContextExecutionBuilder} courant.
	 */
	public ContextExecutionBuilder lireDepuisClassPath(String fichier) {
		instructionsStream = getClass().getResourceAsStream(fichier);
		return this;
	}

	/**
	 * Lit les données depuis une chaîne de caractères. Ceci n'est pas
	 * une opération finale.
	 * 
	 * @param instructions
	 *          Les instructions à interpréter.
	 * @return Le {@link ContextExecutionBuilder} courant.
	 */
	public ContextExecutionBuilder lireDepuisString(String instructions) {
		instructionsStream = new ByteArrayInputStream(instructions.getBytes(charset));
		return this;
	}

	/**
	 * Lit les données depuis un flux. Ceci n'est pas une opération finale.
	 * 
	 * @param instructions
	 *          Le flux contenant les instructions.
	 * @return Le {@link ContextExecutionBuilder} courant.
	 */
	public ContextExecutionBuilder lireDepuisStream(InputStream instructions) {
		instructionsStream = instructions;
		return this;
	}

	/**
	 * Positionne le charset. Par défaut celui-ci est à {@link Charset#UTF_8}.
	 * Ceci n'est pas une opération finale.
	 * 
	 * @param charset
	 *          Le {@link Charset} à utiliser lors de la lecture des instructions.
	 * @return Le {@link ContextExecutionBuilder} courant.
	 */
	public ContextExecutionBuilder setCharset(Charset charset) {
		this.charset = charset;
		return this;
	}

	/**
	 * Création du {@link ContextExecution}. Ceci est une opération finale.
	 * 
	 * @return Le {@link ContextExecution}.
	 * @throws IOException
	 */
	public ContextExecution creerContext() throws IOException {
		ContextExecution context = new ContextExecution();
		List<Interpreteur> initialisationInterpreteurs = initialisationInterpreteurs();
		initialisationInterpreteurs.forEach(i -> i.interpreter(context));
		return context;
	}

	private List<Interpreteur> initialisationInterpreteurs() throws IOException {

		List<Interpreteur> interpreteurs = new ArrayList<Interpreteur>(3);

		try (BufferedReader lecteurFichier = new BufferedReader(new InputStreamReader(instructionsStream, charset))) {

			Spliterator<String> spliterator = lecteurFichier.lines().spliterator();

			boolean continuer = spliterator.tryAdvance(ligne -> interpreteurs.add(new TerrainInterpreteur(ligne)));

			while (continuer) {

				continuer &= spliterator.tryAdvance(ligne -> {
					interpreteurs.add(context -> context.initialiserNouvelleDonneeTondeuse());
					interpreteurs.add(new PositionInitialeInterpreteur(ligne));
				});

				continuer &= spliterator.tryAdvance(ligne -> interpreteurs.add(new InstructionDeplacementInterpreteur(ligne)));
			}
		}

		return interpreteurs;
	}

}
