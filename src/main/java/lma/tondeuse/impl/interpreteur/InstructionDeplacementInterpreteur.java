package lma.tondeuse.impl.interpreteur;

import java.util.ArrayList;
import java.util.List;

import lma.tondeuse.core.instruction.InstructionDeplacement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Interpréteur des instructions de déplacement de la tondeuse.
 * 
 * @author Loïc MARIE
 *
 */
public class InstructionDeplacementInterpreteur implements Interpreteur {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstructionDeplacementInterpreteur.class);

	private String chaineCaracteres;

	/**
	 * Constructeur
	 * 
	 * @param chaineCaracteres
	 *          La chaîne de caractères à interpréter et contenant les
	 *          instructions de déplacement de la tondeuse.
	 */
	public InstructionDeplacementInterpreteur(String chaineCaracteres) {
		this.chaineCaracteres = chaineCaracteres;
	}

	@Override
	public void interpreter(ContextExecution context) {

		List<InstructionDeplacement> listInstruction = new ArrayList<InstructionDeplacement>();

		if (chaineCaracteres == null) {
			LOGGER.warn("Pas d'instruction à traiter");

		} else {

			for (int i = 0; i < chaineCaracteres.length(); i++) {

				char instructionStr = chaineCaracteres.charAt(i);

				try {
					listInstruction.add(InstructionDeplacement.valueOf(Character.toString(instructionStr)));
				} catch (IllegalArgumentException e) {
					LOGGER.warn("Instruction {} non reconnue, elle est donc ignorée", instructionStr);
				}

			}

			LOGGER.debug("Instructions traitées : {}", listInstruction);

			context.setInstructionsDeplacementPourTondeuseCourante(listInstruction);
		}
	}

}
