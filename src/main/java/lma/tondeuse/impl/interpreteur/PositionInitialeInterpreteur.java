package lma.tondeuse.impl.interpreteur;

import static org.apache.commons.lang.Validate.isTrue;
import static org.apache.commons.lang.Validate.notNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;

/**
 * Interpréteur de la position d'une tondeuse.
 * 
 * @author Loïc MARIE
 *
 */
public class PositionInitialeInterpreteur implements Interpreteur {

	private static final Logger LOGGER = LoggerFactory.getLogger(PositionInitialeInterpreteur.class);

	private String chaineCaracteres;

	/**
	 * Constructeur
	 * 
	 * @param chaineCaracteres
	 *          La chaîne de caractères à interpréter et contenant la définition
	 *          de la position initiale de la tondeuse.
	 */
	public PositionInitialeInterpreteur(String chaineCaracteres) {
		this.chaineCaracteres = chaineCaracteres;
	}

	@Override
	public void interpreter(ContextExecution context) {

		notNull(chaineCaracteres);

		String[] elements = chaineCaracteres.split(" ");

		isTrue(elements.length == 3, "Instruction de position non valide ", chaineCaracteres);

		int positionX = Integer.parseInt(elements[0]);
		int positionY = Integer.parseInt(elements[1]);
		Direction direction = Direction.valueOf(elements[2].toUpperCase());

		PositionTondeuse positionInitiale = new PositionTondeuse(positionX, positionY, direction);
		LOGGER.debug("Position initiale traitée : {}", positionInitiale);

		context.setPositionInitialePourTondeuseCourante(positionInitiale);
	}

}
