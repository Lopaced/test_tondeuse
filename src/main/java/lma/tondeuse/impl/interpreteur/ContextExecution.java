package lma.tondeuse.impl.interpreteur;

import static org.apache.commons.lang.Validate.isTrue;

import java.util.ArrayList;
import java.util.Stack;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Le context d'execution de l'interpréteur : cet objet permet de stocker les
 * données interprétées par l'ensemble des {@link Interpreteur}s.
 * 
 * @author Loïc MARIE
 *
 */
public class ContextExecution {

	private class DonneeTondeuse {
		private PositionTondeuse positionInitiale;
		private Iterable<InstructionDeplacement> instructionsDeplacement = new ArrayList<InstructionDeplacement>();
	}

	/** Constructeur scope package */
	ContextExecution() {
	}

	private Stack<DonneeTondeuse> donnesTondeuses = new Stack<DonneeTondeuse>();
	private Terrain terrain;

	/** Initialisation des informations relatives à une nouvelle tondeuse. */
	void initialiserNouvelleDonneeTondeuse() {
		donnesTondeuses.add(new DonneeTondeuse());
	}

	/**
	 * @param terrain
	 *          Le terrain un fois celui-ci interprété.
	 */
	void setTerrain(Terrain terrain) {
		this.terrain = terrain;
	}

	/**
	 * @param positionInitiale
	 *          La position initiale de la tondeuse courante une fois cette
	 *          positon interprétée.
	 */
	void setPositionInitialePourTondeuseCourante(PositionTondeuse positionInitiale) {
		donnesTondeuses.peek().positionInitiale = positionInitiale;
	}

	/**
	 * @param instructionsDeplacement
	 *          Les instruction de déplacement de la tondeuse courante une fois
	 *          celles-ci interprétées.
	 */
	void setInstructionsDeplacementPourTondeuseCourante(Iterable<InstructionDeplacement> instructionsDeplacement) {
		donnesTondeuses.peek().instructionsDeplacement = instructionsDeplacement;
	}

	private DonneeTondeuse getDonneesTondeuses(int tondeuse) {
		isTrue(donnesTondeuses.size() > tondeuse, "Tondeuse inexistante");
		return donnesTondeuses.get(tondeuse);
	}

	/**
	 * Retourne les instructions de déplacement de la tondeuse représentée par
	 * l'indice passé en paramètre.
	 * 
	 * @param tondeuse
	 *          Indice de la tondeuse.
	 * @return Les instructions de déplacement de la tondeuse.
	 */
	public Iterable<InstructionDeplacement> getInstructionsDeplacementPourTondeuse(int tondeuse) {
		return getDonneesTondeuses(tondeuse).instructionsDeplacement;
	}

	/**
	 * Retourne la position initiale de la tondeuse représentée par l'indice passé
	 * en paramètre.
	 * 
	 * @param tondeuse
	 *          Indice de la tondeuse.
	 * @return La position initiale de la tondeuse.
	 */
	public PositionTondeuse getPositionInitialePourTondeuse(int tondeuse) {
		return getDonneesTondeuses(tondeuse).positionInitiale;
	}

	/** @return Retourne le terrain sur lequel évolue la/les tondeuse(s) */
	public Terrain getTerrain() {
		return terrain;
	}

	/** Donne le nombre de tondeuses. */
	public int getNombreTotalTondeuse() {
		return donnesTondeuses.size();
	}

}