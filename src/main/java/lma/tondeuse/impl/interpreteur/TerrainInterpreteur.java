package lma.tondeuse.impl.interpreteur;

import static org.apache.commons.lang.Validate.isTrue;
import static org.apache.commons.lang.Validate.notNull;
import lma.tondeuse.impl.terrain.SimpleTerrainRectangulaire;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Interpréteur de la définition du terrain.
 * 
 * @author Loïc MARIE
 *
 */
public class TerrainInterpreteur implements Interpreteur {

	private static final Logger LOGGER = LoggerFactory.getLogger(TerrainInterpreteur.class);

	private String chaineCaracteres;

	/**
	 * Constructeur
	 * 
	 * @param chaineCaracteres
	 *          La chaîne de caractères à interpréter et contenant la définition
	 *          du terrain.
	 */
	public TerrainInterpreteur(String chaineCaracteres) {
		this.chaineCaracteres = chaineCaracteres;
	}

	@Override
	public void interpreter(ContextExecution context) {

		notNull(chaineCaracteres);

		String[] elements = chaineCaracteres.split(" ");

		isTrue(elements.length == 2, "Instruction de terrain non valide");

		int pointMaxX = Integer.parseInt(elements[0]);
		int pointMaxY = Integer.parseInt(elements[1]);

		// Pour le moment on ne traite que les terrains rectangulaires.
		SimpleTerrainRectangulaire terrain = new SimpleTerrainRectangulaire(pointMaxX, pointMaxY);

		LOGGER.debug("Terrain constrit : {}", terrain);

		context.setTerrain(terrain);

	}

}
