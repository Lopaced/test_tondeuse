package lma.tondeuse.impl.interpreteur;

/**
 * Interface commune aux interpréteurs. Un interpréteur a la responsabilité
 * d'interpréter une chaîne de caractères pour en extraire de l'information et
 * enrichir le {@link ContextExecution}.
 * 
 * @author Loïc MARIE
 *
 */
public interface Interpreteur {

	/**
	 * Démarre le processus d'interprètation. Le résultat est enregistré dans le
	 * context.
	 * 
	 * @param context
	 *          Le context à enrichir.
	 */
	void interpreter(ContextExecution context);
}
