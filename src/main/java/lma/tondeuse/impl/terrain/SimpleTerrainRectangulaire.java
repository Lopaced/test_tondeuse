package lma.tondeuse.impl.terrain;

import static org.apache.commons.lang.Validate.notNull;

import java.util.Formatter;
import java.util.Locale;

import lma.tondeuse.core.position.Position;
import lma.tondeuse.core.terrain.EtatParcelle;
import lma.tondeuse.core.terrain.Terrain;

/**
 * Implementation simple d'un terrain rectangulaire.
 * 
 * @author Loïc MARIE
 *
 */
public class SimpleTerrainRectangulaire implements Terrain {

	private final int maxY;
	private final int maxX;

	private final EtatParcelle[][] etatsParcelles;

	/**
	 * Constructeur d'un terrain rectangulaire.
	 * 
	 * @param maxX
	 *          Coordonnée sur l'axe des abscisses du bord supérieur droit.
	 * @param maxY
	 *          Coordonnée sur l'axe des ordonnées du bord supérieur droit.
	 */
	public SimpleTerrainRectangulaire(int maxX, int maxY) {
		this.maxX = maxX;
		this.maxY = maxY;

		etatsParcelles = new EtatParcelle[maxX + 1][maxY + 1];

		for (int i = 0; i <= maxX; i++) {
			for (int j = 0; j <= maxY; j++) {
				etatsParcelles[i][j] = EtatParcelle.NON_TONDUE;
			}
		}
	}

	@Override
	public Position getBordInferieurGauche() {
		return new Position(0, 0);
	}

	@Override
	public Position getBordSuperieurDroit() {
		return new Position(maxX, maxY);
	}

	private boolean estDansTerrain(final Position position) {
		return position.getX() >= 0 && position.getY() >= 0 && position.getX() <= maxX && position.getY() <= maxY;
	}

	@Override
	public boolean estAccessible(Position position) {
		notNull(position, "position ne doit pas être null");
		return estDansTerrain(position) && getEtatParcelle(position).isAccessible();
	}

	@Override
	public EtatParcelle getEtatParcelle(final Position position) {

		notNull(position, "position ne doit pas être null");

		if (estDansTerrain(position)) {
			return etatsParcelles[position.getX()][position.getY()];
		} else {
			throw new IllegalArgumentException("La position n'est pas dans le terrain");
		}
	}

	@Override
	public void setEtatParcelle(EtatParcelle etat, final Position position) {

		notNull(etat, "etat ne doit pas être null");
		notNull(position, "position ne doit pas être null");

		if (estDansTerrain(position)) {
			etatsParcelles[position.getX()][position.getY()] = etat;
		} else {
			throw new IllegalArgumentException("La position n'est pas dans le terrain");
		}
	}

	@Override
	public String toString() {

		StringBuffer toString = new StringBuffer();

		try (Formatter formatter = new Formatter(toString, Locale.FRANCE)) {

			toString.append("Terrain rectangulaire ");
			toString.append(maxX).append("x").append(maxY).append(" ");

			toString.append("(");
			toString.append("Tondu : X").append("\t");
			toString.append("Non tondu : *").append("\t");
			toString.append("Emplacement tondeuse : T");
			toString.append(")");

			toString.append("\n");
			formatter.format("%4s", "");

			for (int x = 0; x <= maxX; x++) {
				formatter.format("%4d", x);
			}

			for (int y = maxY; y >= 0; y--) {

				formatter.format("\n%4d", y);

				for (int x = 0; x <= maxX; x++) {

					switch (etatsParcelles[x][y]) {
					case NON_TONDUE:
						formatter.format("%4s", "*");
						break;
					case TONDUE:
						formatter.format("%4s", "X");
						break;
					case OCCUPEE:
						formatter.format("%4s", "T");
						break;
					default:
						formatter.format("%4s", "?");
					}
				}
			}
		}

		return toString.toString();
	}

}
