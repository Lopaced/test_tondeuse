package lma.tondeuse;

import static lma.tondeuse.assertions.PositionTondeuseAssertion.assertThat;
import static lma.tondeuse.assertions.TerrainAssertion.assertThat;
import static lma.tondeuse.core.position.Direction.E;
import static lma.tondeuse.core.position.Direction.N;
import static lma.tondeuse.core.terrain.EtatParcelle.NON_TONDUE;
import static lma.tondeuse.core.terrain.EtatParcelle.TONDUE;
import static lma.tondeuse.core.terrain.EtatParcelle.OCCUPEE;

import java.io.FileNotFoundException;
import java.io.IOException;

import lma.tondeuse.core.Tondeuse;
import lma.tondeuse.core.deplacement.DeplacementStrategie;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;
import lma.tondeuse.impl.deplacement.DeplacementStrategieADG;
import lma.tondeuse.impl.interpreteur.ContextExecution;
import lma.tondeuse.impl.interpreteur.ContextExecutionBuilder;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestDeBoutEnBout {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestDeBoutEnBout.class);

	@Test
	public void casDeTest() throws FileNotFoundException, IOException {

		ContextExecutionBuilder builder = new ContextExecutionBuilder();
		ContextExecution context = builder.lireDepuisClassPath("/InstructionsTestDeBoutEnBout").creerContext();

		Terrain terrain = context.getTerrain();
		PositionTondeuse positionInitialeTondeuseA = context.getPositionInitialePourTondeuse(0);
		PositionTondeuse positionInitialeTondeuseB = context.getPositionInitialePourTondeuse(1);

		DeplacementStrategie startegie = new DeplacementStrategieADG();

		Tondeuse tondeuseA = new Tondeuse(terrain, startegie, positionInitialeTondeuseA);
		Tondeuse tondeuseB = new Tondeuse(terrain, startegie, positionInitialeTondeuseB);

		tondeuseA.addDebutInstructionObservateur((i, p) -> LOGGER.info(
		    "Instruction {} à effectuer depuis la position : {}", i, p));

		tondeuseA.addFinInstructionObservateur((instruction, nouvellePosition) -> {
			LOGGER.info("Instruction {} traitée par la tondeuse A, nouvelle position : {}", instruction, nouvellePosition);
			LOGGER.info("Apperçu du terrain : {}", terrain.toString());
		});

		tondeuseA.addFinTraitementObservateur(positionFinale -> {
			LOGGER.info("Fin de déplacement de la tondeuse A, emplacement final : {}", positionFinale);
			assertThat(positionFinale).xEgale(1).yEgale(3).directionEgale(N);
		});

		tondeuseB.addFinTraitementObservateur(positionFinale -> {
			LOGGER.info("Fin de déplacement de la tondeuse B, emplacement final : {}", positionFinale);
			assertThat(positionFinale).xEgale(5).yEgale(1).directionEgale(E);
		});

		// Lancement des instructions
		tondeuseA.effectuerInstructions(context.getInstructionsDeplacementPourTondeuse(0));
		tondeuseB.effectuerInstructions(context.getInstructionsDeplacementPourTondeuse(1));

		// Verification en détail de l'état du terrain
		assertThat(terrain).etatParcelleEgale(0, 0, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(1, 0, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(2, 0, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(3, 0, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(4, 0, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(5, 0, NON_TONDUE);

		assertThat(terrain).etatParcelleEgale(0, 1, TONDUE);
		assertThat(terrain).etatParcelleEgale(1, 1, TONDUE);
		assertThat(terrain).etatParcelleEgale(2, 1, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(3, 1, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(4, 1, TONDUE);
		assertThat(terrain).etatParcelleEgale(5, 1, OCCUPEE);

		assertThat(terrain).etatParcelleEgale(0, 2, TONDUE);
		assertThat(terrain).etatParcelleEgale(1, 2, TONDUE);
		assertThat(terrain).etatParcelleEgale(2, 2, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(3, 2, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(4, 2, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(5, 3, TONDUE);

		assertThat(terrain).etatParcelleEgale(0, 3, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(1, 3, OCCUPEE);
		assertThat(terrain).etatParcelleEgale(2, 3, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(3, 3, TONDUE);
		assertThat(terrain).etatParcelleEgale(4, 3, TONDUE);
		assertThat(terrain).etatParcelleEgale(5, 3, TONDUE);

		assertThat(terrain).etatParcelleEgale(0, 4, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(1, 4, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(2, 4, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(3, 4, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(4, 4, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(5, 4, NON_TONDUE);

		assertThat(terrain).etatParcelleEgale(0, 5, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(1, 5, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(2, 5, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(3, 5, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(4, 5, NON_TONDUE);
		assertThat(terrain).etatParcelleEgale(5, 5, NON_TONDUE);

	}

}
