package lma.tondeuse.core.position;

import static lma.tondeuse.core.position.Direction.E;
import static lma.tondeuse.core.position.Direction.N;
import static lma.tondeuse.core.position.Direction.S;
import static lma.tondeuse.core.position.Direction.W;
import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Test;

public class DirectionTest {

	@Test
	public void testDirectionN() {
		assertThat(N.rotationAntiHoraire()).isEqualTo(W);
		assertThat(N.rotationHoraire()).isEqualTo(E);
	}

	@Test
	public void testDirectionS() {
		assertThat(S.rotationAntiHoraire()).isEqualTo(E);
		assertThat(S.rotationHoraire()).isEqualTo(W);
	}

	@Test
	public void testDirectionE() {
		assertThat(E.rotationAntiHoraire()).isEqualTo(N);
		assertThat(E.rotationHoraire()).isEqualTo(S);
	}

	@Test
	public void testDirectionW() {
		assertThat(W.rotationAntiHoraire()).isEqualTo(S);
		assertThat(W.rotationHoraire()).isEqualTo(N);
	}
}
