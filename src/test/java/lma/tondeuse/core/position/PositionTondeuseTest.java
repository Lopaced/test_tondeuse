package lma.tondeuse.core.position;

import static lma.tondeuse.assertions.PositionTondeuseAssertion.assertThat;
import static lma.tondeuse.core.position.Direction.S;
import lma.tondeuse.core.position.PositionTondeuse;

import org.junit.Test;

public class PositionTondeuseTest extends PositionTest {

	@Test
	public void testConstructeurPosition() {
		PositionTondeuse tested = new PositionTondeuse(5, 8, S);
		assertThat(tested).directionEgale(S).xEgale(5).yEgale(8);
	}

}
