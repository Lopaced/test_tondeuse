package lma.tondeuse.core.position;

import static lma.tondeuse.assertions.PositionAssertion.assertThat;
import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Test;

public class PositionTest {

	@Test
	public void testConstructeurPosition() {
		Position tested = new Position(5, 8);
		assertThat(tested).xEgale(5).yEgale(8);
	}

	@Test
	public void testIsIdenique() {

		Position tested = new Position(5, 8);

		assertThat(tested.isIdentique(new Position(5, 8))).isTrue();
		assertThat(tested.isIdentique(new Position(5, 9))).isFalse();
		assertThat(tested.isIdentique(new Position(6, 8))).isFalse();
		assertThat(tested.isIdentique(null)).isFalse();

	}

	@Test
	public void testToString() {
		assertThat(new Position(5, 8).toString()).isNotNull();
	}

}
