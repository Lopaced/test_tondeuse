package lma.tondeuse.core;

import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.instruction.InstructionDeplacement.D;
import static lma.tondeuse.core.instruction.InstructionDeplacement.G;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import lma.tondeuse.core.deplacement.DeplacementStrategie;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.observateur.DebutInstructionObservateur;
import lma.tondeuse.core.observateur.FinInstructionObservateur;
import lma.tondeuse.core.observateur.FinTraitementObservateur;
import lma.tondeuse.core.position.Position;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.EtatParcelle;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TondeuseTest {

	@Mock
	private Terrain terrainMock;

	@Mock
	private DeplacementStrategie deplacementStrategieMock;

	@Mock
	private PositionTondeuse positionInitialeTondeuse;

	@Mock
	private PositionTondeuse positionTondeuseApresInstructionA, positionTondeuseApresInstructionD,
	    positionTondeuseApresInstructionG;

	private Tondeuse tested;

	@Before
	public void setUp() {

		terrainMock = mock(Terrain.class);
		deplacementStrategieMock = mock(DeplacementStrategie.class);
		positionInitialeTondeuse = mock(PositionTondeuse.class);

		positionTondeuseApresInstructionA = mock(PositionTondeuse.class);
		positionTondeuseApresInstructionD = mock(PositionTondeuse.class);
		positionTondeuseApresInstructionG = mock(PositionTondeuse.class);

		when(terrainMock.estAccessible(any(Position.class))).thenReturn(true);

		when(deplacementStrategieMock.calculerNouvellePosition(eq(A), any(PositionTondeuse.class), any(Terrain.class)))
		    .thenReturn(positionTondeuseApresInstructionA);
		when(deplacementStrategieMock.calculerNouvellePosition(eq(D), any(PositionTondeuse.class), any(Terrain.class)))
		    .thenReturn(positionTondeuseApresInstructionD);
		when(deplacementStrategieMock.calculerNouvellePosition(eq(G), any(PositionTondeuse.class), any(Terrain.class)))
		    .thenReturn(positionTondeuseApresInstructionG);

		tested = new Tondeuse(terrainMock, deplacementStrategieMock, positionInitialeTondeuse);

	}

	@Test
	public void testEffectuerInstructionsCasNominal() {

		tested.effectuerInstructions(Arrays.asList(A, D));

		// 2 appels
		verify(deplacementStrategieMock, times(2)).calculerNouvellePosition(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class), any(Terrain.class));

		// Appel pour instruction A
		verify(deplacementStrategieMock).calculerNouvellePosition(A, positionInitialeTondeuse, terrainMock);

		// Verification de l'état du terrain après instruction A

		verify(terrainMock).setEtatParcelle(eq(EtatParcelle.TONDUE), eq(positionInitialeTondeuse));
		verify(terrainMock).setEtatParcelle(eq(EtatParcelle.OCCUPEE), eq(positionTondeuseApresInstructionA));

		// Appel pour instruction D
		verify(deplacementStrategieMock).calculerNouvellePosition(D, positionTondeuseApresInstructionA, terrainMock);

		// Verification de l'état du terrain après instruction D

		verify(terrainMock).setEtatParcelle(eq(EtatParcelle.TONDUE), eq(positionTondeuseApresInstructionA));
		verify(terrainMock).setEtatParcelle(eq(EtatParcelle.OCCUPEE), eq(positionTondeuseApresInstructionD));

	}

	@Test
	public void testNotificationDebutInstructionObservateurs() {

		DebutInstructionObservateur observateurMock1 = mock(DebutInstructionObservateur.class);
		DebutInstructionObservateur observateurMock2 = mock(DebutInstructionObservateur.class);
		DebutInstructionObservateur observateurMock3 = mock(DebutInstructionObservateur.class);

		tested.addDebutInstructionObservateur(observateurMock1);
		tested.addDebutInstructionObservateur(observateurMock2);
		tested.addDebutInstructionObservateur(observateurMock3);

		tested.removeDebutInstructionObservateur(observateurMock2);

		tested.effectuerInstructions(Arrays.asList(D, G));

		verify(observateurMock1, times(2)).instructionAEffecuter(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class));
		verify(observateurMock1).instructionAEffecuter(eq(D), eq(positionInitialeTondeuse));
		verify(observateurMock1).instructionAEffecuter(eq(G), eq(positionTondeuseApresInstructionD));

		verify(observateurMock2, never()).instructionAEffecuter(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class));

		verify(observateurMock3, times(2)).instructionAEffecuter(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class));
		verify(observateurMock3).instructionAEffecuter(eq(D), eq(positionInitialeTondeuse));
		verify(observateurMock3).instructionAEffecuter(eq(G), eq(positionTondeuseApresInstructionD));
	}

	@Test
	public void testNotificationFinInstructionObservateurs() {

		FinInstructionObservateur observateurMock1 = mock(FinInstructionObservateur.class);
		FinInstructionObservateur observateurMock2 = mock(FinInstructionObservateur.class);
		FinInstructionObservateur observateurMock3 = mock(FinInstructionObservateur.class);

		tested.addFinInstructionObservateur(observateurMock1);
		tested.addFinInstructionObservateur(observateurMock2);
		tested.addFinInstructionObservateur(observateurMock3);

		tested.removeFinInstructionObservateur(observateurMock2);

		tested.effectuerInstructions(Arrays.asList(D, G));

		verify(observateurMock1, times(2)).instructionEffecutee(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class));
		verify(observateurMock1).instructionEffecutee(eq(D), eq(positionTondeuseApresInstructionD));
		verify(observateurMock1).instructionEffecutee(eq(G), eq(positionTondeuseApresInstructionG));

		verify(observateurMock2, never()).instructionEffecutee(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class));

		verify(observateurMock3, times(2)).instructionEffecutee(any(InstructionDeplacement.class),
		    any(PositionTondeuse.class));
		verify(observateurMock3).instructionEffecutee(eq(D), eq(positionTondeuseApresInstructionD));
		verify(observateurMock3).instructionEffecutee(eq(G), eq(positionTondeuseApresInstructionG));
	}

	@Test
	public void testNotificationFinTraitementObservateurs() {

		FinTraitementObservateur observateurMock1 = mock(FinTraitementObservateur.class);
		FinTraitementObservateur observateurMock2 = mock(FinTraitementObservateur.class);
		FinTraitementObservateur observateurMock3 = mock(FinTraitementObservateur.class);

		tested.addFinTraitementObservateur(observateurMock1);
		tested.addFinTraitementObservateur(observateurMock2);
		tested.addFinTraitementObservateur(observateurMock3);

		tested.removeFinTraitementObservateur(observateurMock2);

		tested.effectuerInstructions(Arrays.asList(G, A));

		verify(observateurMock1).traitementEffectue(eq(positionTondeuseApresInstructionA));
		verify(observateurMock2, never()).traitementEffectue(any(PositionTondeuse.class));
		verify(observateurMock3).traitementEffectue(eq(positionTondeuseApresInstructionA));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInitialisationTondeuseSurPositionInaccessible() {
		when(terrainMock.estAccessible(positionInitialeTondeuse)).thenReturn(false);
		new Tondeuse(terrainMock, deplacementStrategieMock, positionInitialeTondeuse);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAjoutDebutInstructionObservateurNull() {
		tested.addDebutInstructionObservateur(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAjoutFinInstructionObservateurNull() {
		tested.addFinInstructionObservateur(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAjoutFinTraitementObservateurNull() {
		tested.addFinTraitementObservateur(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRetraitDebutInstructionObservateurNull() {
		tested.removeDebutInstructionObservateur(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRetraitFinInstructionObservateurNull() {
		tested.removeFinInstructionObservateur(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRetraitFinTraitementObservateurNull() {
		tested.removeFinTraitementObservateur(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEffectuerInstructionsNull() {
		tested.effectuerInstructions(null);
	}
}
