package lma.tondeuse.core.terrain;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Test;

public class EtatParcelleTest {

	@Test
	public void testEstAccessibleNON_TONDUE() {
		assertThat(EtatParcelle.NON_TONDUE.isAccessible()).isTrue();
	}

	@Test
	public void testEstAccessibleOCCUPEE() {
		assertThat(EtatParcelle.OCCUPEE.isAccessible()).isFalse();
	}
	
	@Test
	public void testEstAccessibleTONDUE() {
		assertThat(EtatParcelle.TONDUE.isAccessible()).isTrue();
	}
}
