package lma.tondeuse.assertions;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.impl.interpreteur.ContextExecution;

import org.fest.assertions.api.AbstractAssert;
import org.fest.assertions.api.Assertions;

/**
 * Méthodes pour tester le {@link ContextExecution}.
 * <p>
 * Pour créer une instance de cette classe, utilisez
 * <code>{@link ContextExecutionAssertion#assertThat(ContextExecution)}</code>.
 * </p>
 * 
 * @author Loïc MARIE
 *
 */
public class ContextExecutionAssertion extends AbstractAssert<ContextExecutionAssertion, ContextExecution> {

	/**
	 * Création d'une instance de la classe.
	 * 
	 * @param actual
	 *          Le context d'éxecution à tester.
	 * @return L'instance de la classe de test.
	 */
	public static ContextExecutionAssertion assertThat(ContextExecution actual) {
		return new ContextExecutionAssertion(actual);
	}

	private int tondeuseIndice = -1;

	private ContextExecutionAssertion(ContextExecution actual) {
		super(actual, ContextExecutionAssertion.class);
	}

	/**
	 * Spécifie l'indice de la tondeuse à étudier dans le context.
	 * 
	 * @param indice
	 *          L'indice de la tondeuse.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public ContextExecutionAssertion pourLaTondeuse(int indice) {
		tondeuseIndice = indice;
		return this;
	}

	/**
	 * Retourne la classe utilitaire de test du terrain.
	 * 
	 * @return une instance de la classe utilitaire de test du terrain.
	 */
	public TerrainAssertion verifierTerrain() {
		as("Le ContextExecution est null").isNotNull();
		return TerrainAssertion.assertThat(actual.getTerrain());
	}

	/**
	 * Retourne la classe utilitaire de test de la position pour la tondeuse
	 * courante (spécifiée par <code>{@link #pourLaTondeuse(int)}</code>).
	 * 
	 * @return une instance de la classe utilitaire de test de la position d'une tondeuse.
	 */
	public PositionTondeuseAssertion verifierPositionInitiale() {
		as("Le ContextExecution est null").isNotNull();
		return PositionTondeuseAssertion.assertThat(actual.getPositionInitialePourTondeuse(tondeuseIndice));
	}

	/**
	 * Vérifie que les instructions de déplacement de la tondeuse courante
	 * (spécifiée par <code>{@link #pourLaTondeuse(int)}</code>) sont exactement
	 * identiques à l'attendu.
	 * 
	 * @param attendu
	 *          L'attendu.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public ContextExecutionAssertion instructionsDeplacementEgale(Iterable<InstructionDeplacement> attendu) {
		as("Le ContextExecution est null").isNotNull();
		Assertions.assertThat(actual.getInstructionsDeplacementPourTondeuse(tondeuseIndice)).isEqualTo(attendu);
		return this;
	}

	/**
	 * Vérifie que le nombre total de tondeuses est identique à l'attendu.
	 * 
	 * @param attendu
	 *          Nombre total de tondeuses.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public ContextExecutionAssertion nbDonneesTondeusesEgale(int attendu) {
		as("Le ContextExecution est null").isNotNull();
		Assertions.assertThat(actual.getNombreTotalTondeuse()).isEqualTo(attendu);
		return this;
	}

}