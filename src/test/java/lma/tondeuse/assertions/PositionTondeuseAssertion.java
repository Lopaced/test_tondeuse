package lma.tondeuse.assertions;

import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;

import org.fest.assertions.api.Assertions;

/**
 * Méthodes pour tester une {@link PositionTondeuse}.
 * <p>
 * Pour créer une instance de cette classe, utilisez
 * <code>{@link PositionTondeuseAssertion#assertThat(PositionTondeuse)}</code>.
 * </p>
 * 
 * @author Loïc MARIE
 *
 */
public class PositionTondeuseAssertion extends PositionAssertion {

	/**
	 * Création d'une instance de la classe.
	 * 
	 * @param tested
	 *          La {@link PositionTondeuse} à tester.
	 * @return L'instance de la classe de test.
	 */
	public static PositionTondeuseAssertion assertThat(PositionTondeuse tested) {
		return new PositionTondeuseAssertion(tested);
	}

	private PositionTondeuse tested;

	private PositionTondeuseAssertion(PositionTondeuse tested) {
		super(tested);
		this.tested = tested;
	}

	/**
	 * Test si la direction de la {@link PositionTondeuse} correspond à l'attendu.
	 * 
	 * @param direction
	 *          La direction attendue.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public PositionTondeuseAssertion directionEgale(Direction direction) {

		as("La position est null").isNotNull();
		Assertions.assertThat(tested.getDirection()).as("La direction de la position est inexacte").isEqualTo(direction);

		return this;
	}

	@Override
	public PositionTondeuseAssertion xEgale(int x) {
		return (PositionTondeuseAssertion) super.xEgale(x);
	}

	@Override
	public PositionTondeuseAssertion yEgale(int y) {
		return (PositionTondeuseAssertion) super.yEgale(y);
	}

}
