package lma.tondeuse.assertions;

import lma.tondeuse.core.position.Position;
import lma.tondeuse.core.terrain.EtatParcelle;
import lma.tondeuse.core.terrain.Terrain;

import org.fest.assertions.api.AbstractAssert;
import org.fest.assertions.api.Assertions;

/**
 * Méthodes pour tester un {@link Terrain}.
 * <p>
 * Pour créer une instance de cette classe, utilisez
 * <code>{@link TerrainAssertion#assertThat(Terrain)}</code>.
 * </p>
 * 
 * @author Loïc MARIE
 *
 */
public class TerrainAssertion extends AbstractAssert<TerrainAssertion, Terrain> {

	/**
	 * Création d'une instance de la classe.
	 * 
	 * @param tested
	 *          Le terrain à tester.
	 * @return L'instance de la classe de test.
	 */
	public static TerrainAssertion assertThat(Terrain actual) {
		return new TerrainAssertion(actual);
	}

	private TerrainAssertion(Terrain actual) {
		super(actual, TerrainAssertion.class);
	}

	/**
	 * Test si l'état de la parcelle de coordonnées x et y correspond à l'attendu.
	 * 
	 * @param x
	 *          Coordonnée de la parcelle sur l'axe des abscisses.
	 * @param y
	 *          Coordonnée de la parcelle sur l'axe des ordonnées.
	 * @param etat
	 *          L'état attendu de la parcelle.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public TerrainAssertion etatParcelleEgale(int x, int y, EtatParcelle etat) {

		as("Le terrain est null").isNotNull();
		Assertions.assertThat(actual.getEtatParcelle(new Position(x, y))).isEqualTo(etat);

		return this;
	}

	/**
	 * Test si la coordonnée sur l'axe des abscisses du bord supérieur droit
	 * correspond à l'attendu.
	 * 
	 * @param maxX
	 *          coordonnée attendue.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public TerrainAssertion maxXEgale(int maxX) {

		as("Le terrain est null").isNotNull();
		PositionAssertion.assertThat(actual.getBordSuperieurDroit()).xEgale(maxX);

		return this;
	}

	/**
	 * Test si la coordonnée sur l'axe des ordonnées du bord supérieur droit
	 * correspond à l'attendu.
	 * 
	 * @param maxY
	 *          coordonnée attendue.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public TerrainAssertion maxYEgale(int maxY) {

		as("Le terrain est null").isNotNull();
		PositionAssertion.assertThat(actual.getBordSuperieurDroit()).yEgale(maxY);

		return this;
	}

}
