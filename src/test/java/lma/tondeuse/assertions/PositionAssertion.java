package lma.tondeuse.assertions;

import lma.tondeuse.core.position.Position;

import org.fest.assertions.api.AbstractAssert;
import org.fest.assertions.api.Assertions;

/**
 * Méthodes pour tester une {@link Position}.
 * <p>
 * Pour créer une instance de cette classe, utilisez
 * <code>{@link PositionAssertion#assertThat(Position)}</code>.
 * </p>
 * 
 * @author Loïc MARIE
 *
 */
public class PositionAssertion extends AbstractAssert<PositionAssertion, Position> {

	/**
	 * Création d'une instance de la classe.
	 * 
	 * @param actual
	 *          La position à tester.
	 * @return L'instance de la classe de test.
	 */
	public static PositionAssertion assertThat(Position actual) {
		return new PositionAssertion(actual);
	}

	protected PositionAssertion(Position actual) {
		super(actual, PositionAssertion.class);
	}

	/**
	 * Test si la valeur de la position en abscisse correspond à l'attendu.
	 * 
	 * @param x
	 *          La valeur attendue.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public PositionAssertion xEgale(int x) {

		as("La position est null").isNotNull();
		Assertions.assertThat(actual.getX()).as("Le X de la position est inexacte").isEqualTo(x);

		return this;
	}

	/**
	 * Test si la valeur de la position en ordonnée correspond à l'attendu.
	 * 
	 * @param y
	 *          La valeur attendue.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public PositionAssertion yEgale(int y) {

		as("La position est null").isNotNull();
		Assertions.assertThat(actual.getY()).as("Le Y de la position est inexacte").isEqualTo(y);

		return this;
	}

	/**
	 * Test si la position est identique à l'attendu.
	 * 
	 * @param attendue
	 *          La position attendue.
	 * @return L'instance courante de la classe utilitaire de test.
	 */
	public PositionAssertion estIdentique(Position attendue) {

		xEgale(attendue.getX());
		yEgale(attendue.getY());

		return this;
	}

}
