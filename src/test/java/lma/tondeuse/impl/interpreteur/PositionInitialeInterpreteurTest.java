package lma.tondeuse.impl.interpreteur;

import static lma.tondeuse.assertions.PositionTondeuseAssertion.assertThat;
import static lma.tondeuse.core.position.Direction.E;
import static org.mockito.Mockito.verify;
import lma.tondeuse.core.position.PositionTondeuse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PositionInitialeInterpreteurTest {

	@Mock
	private ContextExecution contextExecutionMock;

	@Test
	public void testCasNominal() {

		PositionInitialeInterpreteur positionInitialeInterpreteur = new PositionInitialeInterpreteur("5 6 E");
		positionInitialeInterpreteur.interpreter(contextExecutionMock);

		ArgumentCaptor<PositionTondeuse> positionTondeuseArgCaptor = ArgumentCaptor.forClass(PositionTondeuse.class);
		verify(contextExecutionMock).setPositionInitialePourTondeuseCourante(positionTondeuseArgCaptor.capture());

		assertThat(positionTondeuseArgCaptor.getValue()).xEgale(5).yEgale(6).directionEgale(E);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFormatIncorrect() {
		PositionInitialeInterpreteur positionInitialeInterpreteur = new PositionInitialeInterpreteur("5 6");
		positionInitialeInterpreteur.interpreter(contextExecutionMock);
	}

}
