package lma.tondeuse.impl.interpreteur;

import static java.util.Arrays.asList;
import static lma.tondeuse.assertions.ContextExecutionAssertion.assertThat;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.instruction.InstructionDeplacement.D;
import static lma.tondeuse.core.instruction.InstructionDeplacement.G;
import static lma.tondeuse.core.position.Direction.E;
import static lma.tondeuse.core.position.Direction.N;

import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

public class ContextExecutionBuilderTest {

	@Test
	public void testCasString() throws Exception {

		String instructions = "5 7\n1 2 N\nGAGAGAGAA\n3 3 E\nAADAADADDA";

		ContextExecutionBuilder tested = new ContextExecutionBuilder();
		ContextExecution context = tested.lireDepuisString(instructions).creerContext();

		assertThat(context).nbDonneesTondeusesEgale(2).verifierTerrain().maxXEgale(5).maxYEgale(7);
		assertThat(context).pourLaTondeuse(0).instructionsDeplacementEgale(asList(G, A, G, A, G, A, G, A, A))
		    .verifierPositionInitiale().directionEgale(N).xEgale(1).yEgale(2);
		assertThat(context).pourLaTondeuse(1).instructionsDeplacementEgale(asList(A, A, D, A, A, D, A, D, D, A))
		    .verifierPositionInitiale().directionEgale(E).xEgale(3).yEgale(3);
	}

	@Test
	public void testCasFichier() throws Exception {

		URL resource = getClass().getResource("/instructionsTest");

		ContextExecutionBuilder tested = new ContextExecutionBuilder();
		ContextExecution context = tested.lireDepuisFichier(new File(resource.getFile())).creerContext();

		assertThat(context).nbDonneesTondeusesEgale(2).verifierTerrain().maxXEgale(5).maxYEgale(5);
		assertThat(context).pourLaTondeuse(0).instructionsDeplacementEgale(asList(G, A, G, A, G, A, G, A, A))
		    .verifierPositionInitiale().directionEgale(N).xEgale(1).yEgale(2);
		assertThat(context).pourLaTondeuse(1).instructionsDeplacementEgale(asList(A, A, D, A, A, D, A, D, D, A))
		    .verifierPositionInitiale().directionEgale(E).xEgale(3).yEgale(3);
	}

	@Test
	public void testCasClasspath() throws Exception {

		ContextExecutionBuilder tested = new ContextExecutionBuilder();
		ContextExecution context = tested.lireDepuisClassPath("/instructionsTest").creerContext();

		assertThat(context).nbDonneesTondeusesEgale(2).verifierTerrain().maxXEgale(5).maxYEgale(5);
		assertThat(context).pourLaTondeuse(0).instructionsDeplacementEgale(asList(G, A, G, A, G, A, G, A, A))
		    .verifierPositionInitiale().directionEgale(N).xEgale(1).yEgale(2);
		assertThat(context).pourLaTondeuse(1).instructionsDeplacementEgale(asList(A, A, D, A, A, D, A, D, D, A))
		    .verifierPositionInitiale().directionEgale(E).xEgale(3).yEgale(3);
	}

	@Test
	public void testCasStream() throws Exception {

		URL resource = getClass().getResource("/instructionsTest");

		ContextExecutionBuilder tested = new ContextExecutionBuilder();
		ContextExecution context = tested.lireDepuisStream(resource.openStream()).creerContext();

		assertThat(context).verifierTerrain().maxXEgale(5).maxYEgale(5);
		assertThat(context).pourLaTondeuse(0).instructionsDeplacementEgale(asList(G, A, G, A, G, A, G, A, A))
		    .verifierPositionInitiale().directionEgale(N).xEgale(1).yEgale(2);
		assertThat(context).pourLaTondeuse(1).instructionsDeplacementEgale(asList(A, A, D, A, A, D, A, D, D, A))
		    .verifierPositionInitiale().directionEgale(E).xEgale(3).yEgale(3);
	}

	@Test
	public void testCasCharsetUTF_16BE() throws Exception {

		ContextExecutionBuilder tested = new ContextExecutionBuilder();
		ContextExecution context = tested.setCharset(StandardCharsets.UTF_16BE)
		    .lireDepuisClassPath("/instructionsTest_UTF16BE").creerContext();

		assertThat(context).verifierTerrain().maxXEgale(5).maxYEgale(5);
		assertThat(context).pourLaTondeuse(0).instructionsDeplacementEgale(asList(G, A, G, A, G, A, G, A, A))
		    .verifierPositionInitiale().directionEgale(N).xEgale(1).yEgale(2);
		assertThat(context).pourLaTondeuse(1).instructionsDeplacementEgale(asList(A, A, D, A, A, D, A, D, D, A))
		    .verifierPositionInitiale().directionEgale(E).xEgale(3).yEgale(3);
	}

	@Test
	public void testCasTondeuseSansDeplacement() throws Exception {

		String instructions = "5 7\n1 2 N\nGAGAGAGAA\n3 3 E";

		ContextExecutionBuilder tested = new ContextExecutionBuilder();
		ContextExecution context = tested.lireDepuisString(instructions).creerContext();

		assertThat(context).nbDonneesTondeusesEgale(2).verifierTerrain().maxXEgale(5).maxYEgale(7);
		assertThat(context).pourLaTondeuse(0).instructionsDeplacementEgale(asList(G, A, G, A, G, A, G, A, A))
		    .verifierPositionInitiale().directionEgale(N).xEgale(1).yEgale(2);
		assertThat(context).pourLaTondeuse(1).instructionsDeplacementEgale(asList()).verifierPositionInitiale()
		    .directionEgale(E).xEgale(3).yEgale(3);
	}

}
