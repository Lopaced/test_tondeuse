package lma.tondeuse.impl.interpreteur;

import static java.util.Arrays.asList;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.instruction.InstructionDeplacement.D;
import static lma.tondeuse.core.instruction.InstructionDeplacement.G;
import static lma.tondeuse.core.instruction.InstructionDeplacement.R;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InstructionDeplacementInterpreteurTest {

	@Mock
	private ContextExecution contextExecutionMock;

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void testCasNominal() {

		InstructionDeplacementInterpreteur tested = new InstructionDeplacementInterpreteur("ADGAGDDR");
		tested.interpreter(contextExecutionMock);

		ArgumentCaptor<Iterable> instructionsArgCaptor = ArgumentCaptor.forClass(Iterable.class);
		verify(contextExecutionMock).setInstructionsDeplacementPourTondeuseCourante(instructionsArgCaptor.capture());

		assertThat(instructionsArgCaptor.getValue()).isEqualTo(asList(A, D, G, A, G, D, D, R));
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testCasSansInstructions() {

		InstructionDeplacementInterpreteur tested = new InstructionDeplacementInterpreteur(null);
		tested.interpreter(contextExecutionMock);

		verify(contextExecutionMock, never()).setInstructionsDeplacementPourTondeuseCourante(Mockito.any(Iterable.class));
	}

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void testCasOmissionInstructionInconne() {

		InstructionDeplacementInterpreteur tested = new InstructionDeplacementInterpreteur("AZERTYDFGHJKWXCV");
		tested.interpreter(contextExecutionMock);

		ArgumentCaptor<Iterable> instructionsArgCaptor = ArgumentCaptor.forClass(Iterable.class);
		verify(contextExecutionMock).setInstructionsDeplacementPourTondeuseCourante(instructionsArgCaptor.capture());

		assertThat(instructionsArgCaptor.getValue()).isEqualTo(asList(A, R, D, G));
	}

}
