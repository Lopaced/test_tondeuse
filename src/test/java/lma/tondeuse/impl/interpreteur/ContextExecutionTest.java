package lma.tondeuse.impl.interpreteur;

import static lma.tondeuse.assertions.ContextExecutionAssertion.assertThat;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.instruction.InstructionDeplacement.D;
import static org.mockito.Mockito.mock;

import java.util.Arrays;

import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContextExecutionTest {

	@Mock
	private Terrain terrainMock;

	private ContextExecution tested;

	@Before
	public void setUp() {
		tested = new ContextExecution();
	}

	@Test
	public void testCreationDonneeTondeuse() throws Exception {

		tested.initialiserNouvelleDonneeTondeuse();
		tested.initialiserNouvelleDonneeTondeuse();
		tested.initialiserNouvelleDonneeTondeuse();

		assertThat(tested).nbDonneesTondeusesEgale(3);
	}

	@Test
	public void testValeursDonneeTondeuse() {

		Iterable<InstructionDeplacement> insrtustions = Arrays.asList(A, D);
		PositionTondeuse position = mock(PositionTondeuse.class);

		tested.initialiserNouvelleDonneeTondeuse();

		tested.setInstructionsDeplacementPourTondeuseCourante(insrtustions);
		tested.setPositionInitialePourTondeuseCourante(position);

		assertThat(tested).pourLaTondeuse(0).instructionsDeplacementEgale(insrtustions).verifierPositionInitiale()
		    .estIdentique(position);
	}

	@Test
	public void testGetTerrain() {
		tested.setTerrain(terrainMock);
		assertThat(tested).verifierTerrain().isEqualTo(terrainMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetDonneeTondeuseInexistant() {
		tested.getInstructionsDeplacementPourTondeuse(0);
	}

}
