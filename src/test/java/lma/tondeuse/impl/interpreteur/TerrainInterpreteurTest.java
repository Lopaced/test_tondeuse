package lma.tondeuse.impl.interpreteur;

import static lma.tondeuse.assertions.TerrainAssertion.assertThat;
import static org.mockito.Mockito.verify;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TerrainInterpreteurTest {

	@Mock
	private ContextExecution contextExecutionMock;

	@Test
	public void testCasNominal() {

		TerrainInterpreteur tested = new TerrainInterpreteur("5 8");
		tested.interpreter(contextExecutionMock);

		ArgumentCaptor<Terrain> terrainArgCaptor = ArgumentCaptor.forClass(Terrain.class);
		verify(contextExecutionMock).setTerrain(terrainArgCaptor.capture());

		assertThat(terrainArgCaptor.getValue()).maxXEgale(5).maxYEgale(8);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFormatNumeriqueIncorrect() {
		TerrainInterpreteur tested = new TerrainInterpreteur("5 A");
		tested.interpreter(contextExecutionMock);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFormatIncorrect() {
		TerrainInterpreteur tested = new TerrainInterpreteur("5 6 6");
		tested.interpreter(contextExecutionMock);
	}

}
