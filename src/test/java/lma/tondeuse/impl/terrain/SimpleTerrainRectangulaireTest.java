package lma.tondeuse.impl.terrain;

import static lma.tondeuse.assertions.PositionAssertion.assertThat;
import static lma.tondeuse.assertions.TerrainAssertion.assertThat;
import static lma.tondeuse.core.terrain.EtatParcelle.NON_TONDUE;
import static lma.tondeuse.core.terrain.EtatParcelle.OCCUPEE;
import static lma.tondeuse.core.terrain.EtatParcelle.TONDUE;
import static org.fest.assertions.api.Assertions.assertThat;
import lma.tondeuse.core.position.Position;

import org.junit.Test;

public class SimpleTerrainRectangulaireTest {

	@Test
	public void testPositionBordsTerrain() {

		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);

		assertThat(tested.getBordInferieurGauche()).xEgale(0).yEgale(0);
		assertThat(tested.getBordSuperieurDroit()).xEgale(5).yEgale(6);

	}

	@Test
	public void testIsPositionTerrainAvecPositionDansLimite() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		assertThat(tested.estAccessible(new Position(5, 6))).isTrue();
	}

	@Test
	public void testIsPositionTerrainAvecPositionNonAccessible() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		Position position = new Position(5, 6);

		tested.setEtatParcelle(OCCUPEE, position);
		assertThat(tested.estAccessible(position)).isFalse();

		tested.setEtatParcelle(TONDUE, position);
		assertThat(tested.estAccessible(position)).isTrue();
	}

	@Test
	public void testIsPositionTerrainAvecPositionHorsLimite() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		assertThat(tested.estAccessible(new Position(6, 0))).isFalse();
		assertThat(tested.estAccessible(new Position(0, 7))).isFalse();
		assertThat(tested.estAccessible(new Position(-1, 1))).isFalse();
		assertThat(tested.estAccessible(new Position(1, -1))).isFalse();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIsDansTerrainAvecParametreNull() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		assertThat(tested.estAccessible(null));
	}

	@Test
	public void testVerificationEtatInitalParcelles() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);

		for (int x = 0; x <= tested.getBordSuperieurDroit().getX(); x++) {
			for (int y = 0; y <= tested.getBordSuperieurDroit().getY(); y++) {
				assertThat(tested).etatParcelleEgale(x, y, NON_TONDUE);
			}
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testgetEtatParcelleAvecParamNull() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		tested.getEtatParcelle(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testgetEtatParcelleAvecPositionHorsTerrain() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		tested.getEtatParcelle(new Position(8, 3));
	}

	@Test
	public void testVerificationModificationEtatParcelles() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);

		Position positionTondueOccupee = new Position(3, 4);
		Position positionTondue = new Position(1, 2);

		tested.setEtatParcelle(OCCUPEE, positionTondueOccupee);
		tested.setEtatParcelle(TONDUE, positionTondue);

		for (int x = 0; x <= 5; x++) {
			for (int y = 0; y <= 5; y++) {

				if (x == positionTondue.getX() && y == positionTondue.getY()) {
					assertThat(tested).etatParcelleEgale(x, y, TONDUE);

				} else if (x == positionTondueOccupee.getX() && y == positionTondueOccupee.getY()) {
					assertThat(tested).etatParcelleEgale(x, y, OCCUPEE);

				} else {
					assertThat(tested).etatParcelleEgale(x, y, NON_TONDUE);
				}
			}
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testVerificationModificationEtatParcellesAvecParamPositionNull() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		tested.setEtatParcelle(TONDUE, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testVerificationModificationEtatParcellesAvecParamEtatNull() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		tested.setEtatParcelle(null, new Position(1, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testVerificationModificationEtatParcellesAvecPositionHorsTerrain() {
		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		tested.setEtatParcelle(TONDUE, new Position(8, 2));
	}

	@Test
	public void testVerifierToString() {

		SimpleTerrainRectangulaire tested = new SimpleTerrainRectangulaire(5, 6);
		tested.setEtatParcelle(OCCUPEE, new Position(1, 2));
		tested.setEtatParcelle(TONDUE, new Position(1, 3));

		assertThat(tested.toString()).isNotEmpty();
		assertThat(tested.toString().length()).isGreaterThan(5 * 6);
	}

}
