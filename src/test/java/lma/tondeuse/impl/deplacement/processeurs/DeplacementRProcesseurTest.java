package lma.tondeuse.impl.deplacement.processeurs;

import static lma.tondeuse.core.instruction.InstructionDeplacement.G;
import static lma.tondeuse.core.instruction.InstructionDeplacement.R;
import static lma.tondeuse.core.position.Direction.N;
import static org.fest.assertions.api.Assertions.assertThat;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeplacementRProcesseurTest {

	@Mock
	private Terrain terrainMock;

	private DeplacementRProcesseur tested;

	@Before
	public void setUp() {
		tested = new DeplacementRProcesseur();
	}

	@Test
	public void peutTraiterInstructionR() throws Exception {

		for (InstructionDeplacement instruction : InstructionDeplacement.values()) {
			if (instruction == R) {
				assertThat(tested.peutTraiter(instruction)).isTrue();
			} else {
				assertThat(tested.peutTraiter(instruction)).isFalse();
			}
		}
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testInstructionD() {

		Direction directionInitiale = N;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		tested.calculerFuturePosition(terrainMock, G, positionInitiale);
	}
}
