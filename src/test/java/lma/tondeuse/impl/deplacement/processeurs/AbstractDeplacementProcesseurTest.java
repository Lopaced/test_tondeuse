package lma.tondeuse.impl.deplacement.processeurs;

import static lma.tondeuse.assertions.PositionAssertion.assertThat;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.position.Direction.N;
import static lma.tondeuse.core.position.Direction.S;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Position;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;

public class AbstractDeplacementProcesseurTest {

	private PositionTondeuse expectedPosition;
	private PositionTondeuse positionInitiale;
	private Terrain terrainMock;

	@Before
	public void setUp() {

		positionInitiale = new PositionTondeuse(0, 0, S);
		expectedPosition = new PositionTondeuse(10, 12, N);

		terrainMock = mock(Terrain.class);
		when(terrainMock.estAccessible(any(Position.class))).thenReturn(true);

	}

	@Test
	public void testCasNominalAvec2Processeurs() {

		AbstractDeplacementProcesseur processeur1 = new AbstractDeplacementProcesseur() {

			@Override
			protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
				return false;
			}

			@Override
			protected PositionTondeuse calculerFuturePosition(final Terrain terrain, InstructionDeplacement instructionDeplacement,
					PositionTondeuse positionInitiale) {
				throw new UnsupportedOperationException("calculerFuturePosition ne doit pas être appelée par processeur1");
			}

		};

		AbstractDeplacementProcesseur processeur2 = new AbstractDeplacementProcesseur() {

			@Override
			protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
				return true;
			}

			@Override
			protected PositionTondeuse calculerFuturePosition(final Terrain terrain, InstructionDeplacement instructionDeplacement,
					PositionTondeuse positionInitiale) {
				return expectedPosition;
			}

		};

		processeur1.setProcesseurSuivant(processeur2);

		Position nouvellePosition = processeur1.calculerDeplacement(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).isSameAs(expectedPosition);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInstructionNonTraitee() {

		AbstractDeplacementProcesseur tested = new AbstractDeplacementProcesseur() {

			@Override
			protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
				return false;
			}

			@Override
			protected PositionTondeuse calculerFuturePosition(final Terrain terrain, InstructionDeplacement instructionDeplacement,
					PositionTondeuse positionInitiale) {
				throw new UnsupportedOperationException("calculerFuturePosition ne doit pas être appelée ici");
			}

		};

		tested.calculerDeplacement(terrainMock, A, positionInitiale);

	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAjoutProcesseurSuivantNull() {
		AbstractDeplacementProcesseur tested = new AbstractDeplacementProcesseur() {

			@Override
			protected boolean peutTraiter(InstructionDeplacement instructionDeplacement) {
				throw new UnsupportedOperationException("calculerFuturePosition ne doit pas être appelée ici");
			}

			@Override
			protected PositionTondeuse calculerFuturePosition(final Terrain terrain, InstructionDeplacement instructionDeplacement,
					PositionTondeuse positionInitiale) {
				throw new UnsupportedOperationException("calculerFuturePosition ne doit pas être appelée ici");
			}

		};

		tested.setProcesseurSuivant(null);
	}


}
