package lma.tondeuse.impl.deplacement.processeurs;

import static lma.tondeuse.assertions.PositionTondeuseAssertion.assertThat;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.position.Direction.E;
import static lma.tondeuse.core.position.Direction.N;
import static lma.tondeuse.core.position.Direction.S;
import static lma.tondeuse.core.position.Direction.W;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.Position;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeplacementAProcesseurTest {

	private static final int POSITION_INITIALE_Y = 2;
	private static final int POSITION_INITIALE_X = 3;

	@Mock
	private Terrain terrainMock;

	private DeplacementAProcesseur tested;

	@Before
	public void setUp() {
		tested = new DeplacementAProcesseur();
		when(terrainMock.estAccessible(any(Position.class))).thenReturn(true);
	}

	@Test
	public void peutTraiterInstructionA() throws Exception {

		for (InstructionDeplacement instruction : InstructionDeplacement.values()) {
			if (instruction == A) {
				assertThat(tested.peutTraiter(instruction)).isTrue();
			} else {
				assertThat(tested.peutTraiter(instruction)).isFalse();
			}
		}
	}

	@Test
	public void testInstructionAQuandDirectionN() {

		Direction directionInitiale = N;
		PositionTondeuse positionInitiale = new PositionTondeuse(POSITION_INITIALE_X, POSITION_INITIALE_Y, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(POSITION_INITIALE_X).yEgale(POSITION_INITIALE_Y + 1).directionEgale(directionInitiale);
	}

	@Test
	public void testInstructionAQuandDirectionS() {

		Direction directionInitiale = S;
		PositionTondeuse positionInitiale = new PositionTondeuse(POSITION_INITIALE_X, POSITION_INITIALE_Y, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(POSITION_INITIALE_X).yEgale(POSITION_INITIALE_Y - 1).directionEgale(directionInitiale);
	}

	@Test
	public void testInstructionAQuandDirectionW() {

		Direction directionInitiale = W;
		PositionTondeuse positionInitiale = new PositionTondeuse(POSITION_INITIALE_X, POSITION_INITIALE_Y, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(POSITION_INITIALE_X - 1).yEgale(POSITION_INITIALE_Y).directionEgale(directionInitiale);
	}

	@Test
	public void testInstructionAQuandDirectionE() {

		Direction directionInitiale = E;
		PositionTondeuse positionInitiale = new PositionTondeuse(POSITION_INITIALE_X, POSITION_INITIALE_Y, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(POSITION_INITIALE_X + 1).yEgale(POSITION_INITIALE_Y).directionEgale(directionInitiale);
	}

	@Test
	public void testInstructionAQuandSortieDuTerrain() {

		when(terrainMock.estAccessible(any(Position.class))).thenReturn(false);

		Direction directionInitiale = E;
		PositionTondeuse positionInitiale = new PositionTondeuse(POSITION_INITIALE_X, POSITION_INITIALE_Y, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(POSITION_INITIALE_X).yEgale(POSITION_INITIALE_Y).directionEgale(directionInitiale);
	}
}
