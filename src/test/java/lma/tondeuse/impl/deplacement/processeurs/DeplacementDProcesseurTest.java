package lma.tondeuse.impl.deplacement.processeurs;

import static lma.tondeuse.assertions.PositionTondeuseAssertion.assertThat;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.instruction.InstructionDeplacement.D;
import static lma.tondeuse.core.position.Direction.E;
import static lma.tondeuse.core.position.Direction.N;
import static lma.tondeuse.core.position.Direction.S;
import static lma.tondeuse.core.position.Direction.W;
import static org.fest.assertions.api.Assertions.assertThat;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeplacementDProcesseurTest {

	@Mock
	private Terrain terrainMock;
	
	private DeplacementDProcesseur tested;

	@Before
	public void setUp() {
		tested = new DeplacementDProcesseur();
	}

	@Test
	public void peutTraiterInstructionD() throws Exception {

		for (InstructionDeplacement instruction : InstructionDeplacement.values()) {
			if (instruction == D) {
				assertThat(tested.peutTraiter(instruction)).isTrue();
			} else {
				assertThat(tested.peutTraiter(instruction)).isFalse();
			}
		}
	}

	@Test
	public void testInstructionDQuandDirectionN() {

		Direction directionInitiale = N;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(E);
	}

	@Test
	public void testInstructionDQuandDirectionS() {

		Direction directionInitiale = S;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(W);
	}

	@Test
	public void testInstructionDQuandDirectionW() {

		Direction directionInitiale = W;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(N);
	}

	@Test
	public void testInstructionDQuandDirectionE() {

		Direction directionInitiale = E;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, A, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(S);
	}

}
