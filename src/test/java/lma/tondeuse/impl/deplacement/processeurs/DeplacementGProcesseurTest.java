package lma.tondeuse.impl.deplacement.processeurs;

import static lma.tondeuse.assertions.PositionTondeuseAssertion.assertThat;
import static lma.tondeuse.core.instruction.InstructionDeplacement.G;
import static lma.tondeuse.core.position.Direction.E;
import static lma.tondeuse.core.position.Direction.N;
import static lma.tondeuse.core.position.Direction.S;
import static lma.tondeuse.core.position.Direction.W;
import static org.fest.assertions.api.Assertions.assertThat;
import lma.tondeuse.core.instruction.InstructionDeplacement;
import lma.tondeuse.core.position.Direction;
import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeplacementGProcesseurTest {

	@Mock
	private Terrain terrainMock;
	
	private DeplacementGProcesseur tested;

	@Before
	public void setUp() {
		tested = new DeplacementGProcesseur();
	}

	@Test
	public void peutTraiterInstructionG() throws Exception {

		for (InstructionDeplacement instruction : InstructionDeplacement.values()) {
			if (instruction == G) {
				assertThat(tested.peutTraiter(instruction)).isTrue();
			} else {
				assertThat(tested.peutTraiter(instruction)).isFalse();
			}
		}
	}

	@Test
	public void testInstructionGQuandDirectionN() {

		Direction directionInitiale = N;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, G, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(W);
	}

	@Test
	public void testInstructionGQuandDirectionS() {

		Direction directionInitiale = S;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, G, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(E);
	}

	@Test
	public void testInstructionGQuandDirectionW() {

		Direction directionInitiale = W;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, G, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(S);
	}

	@Test
	public void testInstructionGQuandDirectionE() {

		Direction directionInitiale = E;
		PositionTondeuse positionInitiale = new PositionTondeuse(0, 0, directionInitiale);

		PositionTondeuse nouvellePosition = tested.calculerFuturePosition(terrainMock, G, positionInitiale);

		assertThat(nouvellePosition).xEgale(0).yEgale(0).directionEgale(N);
	}
}
