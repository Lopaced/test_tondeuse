package lma.tondeuse.impl.deplacement;

import static java.util.Arrays.asList;
import static lma.tondeuse.core.instruction.InstructionDeplacement.A;
import static lma.tondeuse.core.instruction.InstructionDeplacement.R;
import static lma.tondeuse.core.position.Direction.N;
import static org.apache.commons.lang3.reflect.FieldUtils.readField;
import static org.apache.commons.lang3.reflect.FieldUtils.writeField;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import lma.tondeuse.core.position.PositionTondeuse;
import lma.tondeuse.core.terrain.Terrain;
import lma.tondeuse.impl.deplacement.DeplacementStrategieADG;
import lma.tondeuse.impl.deplacement.processeurs.AbstractDeplacementProcesseur;
import lma.tondeuse.impl.deplacement.processeurs.DeplacementAProcesseur;
import lma.tondeuse.impl.deplacement.processeurs.DeplacementDProcesseur;
import lma.tondeuse.impl.deplacement.processeurs.DeplacementGProcesseur;

import org.junit.Before;
import org.junit.Test;

public class DeplacementStrategieADGTest {

	private DeplacementStrategieADG tested;
	private Terrain terrainMock;
	private PositionTondeuse position;

	@Before
	public void setUp() {
		tested = new DeplacementStrategieADG();
		terrainMock = mock(Terrain.class);
		position = new PositionTondeuse(0, 0, N);

		when(terrainMock.estAccessible(any(PositionTondeuse.class))).thenReturn(true);
	}

	@Test
	public void verifierEnsembleProcesseurs() throws IllegalAccessException {

		AbstractDeplacementProcesseur processeur = (AbstractDeplacementProcesseur) readField(tested, "deplacementProcesseurChaine", true);

		assertThat(processeur).isNotNull();

		List<Class<?>> processorsObtenus = new ArrayList<Class<?>>();
		processorsObtenus.add(processeur.getClass());

		while (processeur.getProcesseurSuivant() != null) {
			processorsObtenus.add(processeur.getProcesseurSuivant().getClass());
			processeur = processeur.getProcesseurSuivant();
		}

		assertThat(processorsObtenus).isEqualTo(asList(DeplacementAProcesseur.class, DeplacementGProcesseur.class, DeplacementDProcesseur.class));
	}

	@Test
	public void casCasNominal() throws IllegalAccessException {

		AbstractDeplacementProcesseur processeurMock = mock(AbstractDeplacementProcesseur.class);
		PositionTondeuse expectedPosition = new PositionTondeuse(10, 12, N);

		when(processeurMock.calculerDeplacement(eq(terrainMock), eq(A), eq(position))).thenReturn(expectedPosition);

		writeField(tested, "deplacementProcesseurChaine", processeurMock, true);

		PositionTondeuse nouvellePosition = tested.calculerNouvellePosition(A, position, terrainMock);
		assertThat(nouvellePosition).isEqualTo(expectedPosition);

	}

	@Test(expected = IllegalArgumentException.class)
	public void casInstructionDeplacementNull() {
		tested.calculerNouvellePosition(null, position, terrainMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void casPositionNull() {
		tested.calculerNouvellePosition(A, null, terrainMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void casTerrainNull() {
		tested.calculerNouvellePosition(A, position, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void casInstructionNonValide() {
		tested.calculerNouvellePosition(R, position, terrainMock);
	}
}
